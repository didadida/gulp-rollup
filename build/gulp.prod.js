const config = require('./gulp.config')
const gulp = require('gulp')
const plumber = require('gulp-plumber');
const stylus = require('gulp-stylus')
const gulpIf = require('gulp-if')
const concat = require('gulp-concat')
const notify = require('gulp-notify')
const del = require('del')
const cleanCss = require('gulp-clean-css')
const rename = require("gulp-rename");
const commonjs = require("rollup-plugin-commonjs");
const resolve = require("rollup-plugin-node-resolve");
const babel = require("rollup-plugin-babel");
const cleanup = require("rollup-plugin-cleanup");
const rollup = require("gulp-rollup-each");
const uglify = require("gulp-uglify");
const err = e => plumber({errorHandler: notify.onError('Error: <%= error.message %>')})
const html = () => gulp
  .src(config.html.src)
  .pipe(err())
  .pipe(gulp.dest(config.html.dist))

const css = () => gulp
  .src(config.css.src)
  .pipe(err())
  .pipe(eval(config.css.engin)())
  .pipe(gulpIf(f => config.css.merge && !config.css.exclude.find(v => f.relative.includes(v)), concat(config.css.mergeName || 'app.min.css')))
  .pipe(gulpIf(f => !config.css.exclude.find(v => f.relative.includes(v)), cleanCss()))
  .pipe(gulp.dest(config.css.dist))

const js = () => gulp
  .src(config.js.src)
  .pipe(err())
  .pipe(gulpIf(f => !config.js.exclude.find(v => f.relative.includes(v)), rollup({
    output: {
      name: "aaa.js",
      format: "umd",
      globals: {
        lodash: "lodash",
        $: "$"
      }
    },
    plugins: [
      resolve({
        jsnext: true,
        main: true,
        brower: true
      }),
      commonjs(),
      babel({
        exclude: "node_modules/**",
        runtimeHelpers: true,
        externalHelpers: false,
        extensions: [".js", ".ts"],
      }),
      cleanup()
    ],
    external: ["lodash", "zepto"]
  })))
  .pipe(rename({extname:'.js'}))
  .pipe(gulpIf(f => config.js.compress && !config.js.exclude.find(v => f.relative.includes(v)), uglify()))
  .pipe(gulp.dest(config.js.dist))

const images = () => gulp
  .src(config.images.src)
  .pipe(gulp.dest(config.images.dist))

const copy = () => gulp
  .src(config.copy.src, {base: config.src})
  .pipe(gulp.dest(config.copy.dist))

const remove = () => del(config.dist)

module.exports = function () {
    gulp.task('default', gulp.series(remove, html, css, js, images, copy))
}

