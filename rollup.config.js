import commonjs from "rollup-plugin-commonjs";
import resolve from "rollup-plugin-node-resolve";
import babel from "rollup-plugin-babel";
import cleanup from "rollup-plugin-cleanup";
export default {
  input: 'src/main.ts',
  output: {
    file: "dist/[name].js",
    format: "umd",
    globals: {
      lodash: "lodash",
      zepto: "$"
    }
  },
  plugins: [
    resolve({
      jsnext: true,
      main: true,
      brower: true
    }),
    commonjs(),
    babel({
      exclude: "node_modules/**",
      runtimeHelpers: true,
      externalHelpers: false,
      extensions: [".js", ".ts"]
    }),
    cleanup()
  ],
  external: ["lodash", "zepto"]
};
