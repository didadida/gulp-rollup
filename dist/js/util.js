(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}(function () { 'use strict';

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }
  var classCallCheck = _classCallCheck;

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }
  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }
  var createClass = _createClass;

  var config = window.config = {
    pageSize: 6,
    baseUrl: "http://test.zcmy168.com"
  };
  var isFunction = function isFunction(variable) {
    return typeof variable === "function";
  };
  var PENDING = "PENDING";
  var FULFILLED = "FULFILLED";
  var REJECTED = "REJECTED";
  var MyPromise =
  function () {
    function MyPromise(handle) {
      classCallCheck(this, MyPromise);
      if (!isFunction(handle)) {
        throw new Error("MyPromise must accept a function as a parameter");
      }
      this._status = PENDING;
      this._value = undefined;
      this._fulfilledQueues = [];
      this._rejectedQueues = [];
      try {
        handle(this._resolve.bind(this), this._reject.bind(this));
      } catch (err) {
        this._reject(err);
      }
    }
    createClass(MyPromise, [{
      key: "_resolve",
      value: function _resolve(val) {
        var _this = this;
        var run = function run() {
          if (_this._status !== PENDING) return;
          var runFulfilled = function runFulfilled(value) {
            var cb;
            while (cb = _this._fulfilledQueues.shift()) {
              cb(value);
            }
          };
          var runRejected = function runRejected(error) {
            var cb;
            while (cb = _this._rejectedQueues.shift()) {
              cb(error);
            }
          };
          if (val instanceof MyPromise) {
            val.then(function (value) {
              _this._value = value;
              _this._status = FULFILLED;
              runFulfilled(value);
            }, function (err) {
              _this._value = err;
              _this._status = REJECTED;
              runRejected(err);
            });
          } else {
            _this._value = val;
            _this._status = FULFILLED;
            runFulfilled(val);
          }
        };
        setTimeout(run, 0);
      }
    }, {
      key: "_reject",
      value: function _reject(err) {
        var _this2 = this;
        if (this._status !== PENDING) return;
        var run = function run() {
          _this2._status = REJECTED;
          _this2._value = err;
          var cb;
          while (cb = _this2._rejectedQueues.shift()) {
            cb(err);
          }
        };
        setTimeout(run, 0);
      }
    }, {
      key: "then",
      value: function then(onFulfilled, onRejected) {
        var _this3 = this;
        var _value = this._value,
            _status = this._status;
        return new MyPromise(function (onFulfilledNext, onRejectedNext) {
          var fulfilled = function fulfilled(value) {
            try {
              if (!isFunction(onFulfilled)) {
                onFulfilledNext(value);
              } else {
                var res = onFulfilled(value);
                if (res instanceof MyPromise) {
                  res.then(onFulfilledNext, onRejectedNext);
                } else {
                  onFulfilledNext(res);
                }
              }
            } catch (err) {
              onRejectedNext(err);
            }
          };
          var rejected = function rejected(error) {
            try {
              if (!isFunction(onRejected)) {
                onRejectedNext(error);
              } else {
                var res = onRejected(error);
                if (res instanceof MyPromise) {
                  res.then(onFulfilledNext, onRejectedNext);
                } else {
                  onFulfilledNext(res);
                }
              }
            } catch (err) {
              onRejectedNext(err);
            }
          };
          switch (_status) {
            case PENDING:
              _this3._fulfilledQueues.push(fulfilled);
              _this3._rejectedQueues.push(rejected);
              break;
            case FULFILLED:
              fulfilled(_value);
              break;
            case REJECTED:
              rejected(_value);
              break;
          }
        });
      }
    }, {
      key: "catch",
      value: function _catch(onRejected) {
        return this.then(undefined, onRejected);
      }
    }], [{
      key: "resolve",
      value: function resolve(value) {
        if (value instanceof MyPromise) return value;
        return new MyPromise(function (resolve) {
          return resolve(value);
        });
      }
    }, {
      key: "reject",
      value: function reject(value) {
        return new MyPromise(function (resolve, reject) {
          return reject(value);
        });
      }
    }]);
    return MyPromise;
  }();
  window.Promise = window.Promise || MyPromise;
  var bodyEl = $(document.body);
  var modal = window.modal = {
    point: null,
    base: function base(cfg) {
      if (this.point) {
        this.cancel(this.point);
      }
      var modal = $('<div class="fixed full-width full-height left top row bg-translucence modal text-center"></div>');
      var dom = "<div class=\"self-center bg-white round-xs\">\n                <div class=\"modal-body pd-sm line-1\">";
      if (cfg.type === "loading") {
        dom += "<div class=\"loading row justify-between self-center\"><i class=\"iconfont iconloading fg-black font-24\"></i></div></div>";
      } else if (cfg.type === "confirm") {
        dom += "".concat(cfg.msg, "</div>\n                <div class=\"modal-footer row btn-group\">\n                    <button class=\"btn bg-orange col-6\" onclick=\"modal.cancel_cb()\">\u53D6\u6D88</button>\n                    <button class=\"btn bg-green col-6\" onclick=\"modal.ok()\">\u786E\u5B9A</button>\n                </div>");
        this.cancel = cfg.cancel;
        this.confirm = cfg.confirm;
      }
      dom += "</div>\n        </div>";
      modal.append(dom);
      bodyEl.append(modal);
      setTimeout(function () {
        return modal.addClass("show");
      }, 60);
      this.point = modal;
    },
    cancel_cb: function cancel_cb(el) {
      var _this4 = this;
      el = el || this.point;
      el.removeClass("show");
      this.cancel && this.cancel();
      setTimeout(function () {
        el.remove();
        el = _this4.point = null;
      }, 200);
    },
    cancel: null,
    ok: null
  };
  $.loading = function () {
    return modal.base({
      type: "loading"
    });
  };
  $.confirm = function (cfg) {
    return modal.base({
      type: "confirm",
      msg: cfg.msg || "",
      cancel: cfg.cancel,
      ok: cfg.ok
    });
  };
  $.hide = function () {
    return modal.cancel_cb(modal.point);
  };
  var toast = window.toast = {
    el: null,
    base: function base(flag, msg, time) {
      var _this5 = this;
      msg = msg || (flag ? "操作成功" : "操作失败");
      if (this.el) {
        this.el.remove();
      }
      this.el = $("<div class=\"notice fixed left top full-width row items-center pad-sm-x pad-xs-y shadow-xs-down ellipsis bg-white justify-center\"></div>");
      this.el.append("<i class=\"pad-xs-r font-20 iconfont ".concat(flag ? "iconchenggong fg-green" : "icontishi fg-orange", "\"></i>\n        <span>&nbsp;").concat(msg, "</span>"));
      bodyEl.append(this.el);
      setTimeout(function () {
        return _this5.el.addClass("show");
      });
      setTimeout(function () {
        return _this5.cancel();
      }, time || 2500);
    },
    cancel: function cancel(el) {
      el = el || this.el;
      el.removeClass("show");
      setTimeout(function () {
        return el.remove();
      }, 200);
    }
  };
  $.success = function (msg, time) {
    return toast.base(1, msg, time);
  };
  $.error = function (msg, time) {
    return toast.base(0, msg, time);
  };
  $.fetch = function () {
    var o = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    return new Promise(function (resolve, reject) {
      $.ajax({
        timeout: 20000,
        type: o.type || "GET",
        url: config.baseUrl + o.url,
        data: o.data || {},
        dataType: "json",
        contentType: "application/json",
        success: function success(data, status, xhr) {
          if (data.code === 1) {
            resolve(data.data);
          } else {
            $.error(data.msg);
            reject(data);
          }
        },
        error: function error(xhr, errorType, _error) {
          if (errorType === "timeout") return $.confirm({
            msg: "请求失败，是否重试？",
            ok: function ok() {
              $.loading();
              $.fetch(o);
            }
          });
          $.error("网络错误！");
        }
      });
    });
  };
  $.getUrlParams = function () {
    var params = {};
    var _params = window.location.search.replace("?", "").split("&").forEach(function (v) {
      var kv = v.split("=");
      params[kv[0]] = kv[1];
    });
    return params;
  };

}));
