(function (factory) {
  typeof define === 'function' && define.amd ? define(factory) :
  factory();
}(function () { 'use strict';

  var action = window.action = {
    slider: $(".slide-box"),
    products: $(".products"),
    productsBtn: $(".products-btn"),
    cases: $(".cases"),
    casesBtn: $(".cases-btn"),
    productNo: 0,
    caseNo: 0,
    loadingHTML: "<i class=\"iconfont iconloading inline mg-xs-r\"></i>\u6B63\u5728\u52A0\u8F7D...",
    loadedHTML: "\u67E5\u770B\u66F4\u591A",
    renderSlider: function renderSlider(data) {
      this.slider.html(data.map(function (v, i) {
        return "\n            <li class=\"absolute left top full-width full-height\">\n                <a href=\"".concat(v.clickUrl, "\">\n                <img ").concat(i == 0 ? "" : "data-", "src=\"").concat(v.imgUrl, "\"/>\n                </a>\n            </li>\n            ");
      }).join(""));
      setTimeout(function () {
        $("#slide").swipeSlide({
          autoSwipe: true,
          continuousScroll: true,
          lazyLoad: true
        });
      }, 0);
    },
    renderProducts: function renderProducts(data) {
      this.products.append(data.rows.map(function (v) {
        return "<a href=\"/productInfo.html?id=".concat(v.id, "\" class=\"col-4 col-md-3 col-lg-2\">\n                    <figure class=\"font-0\">\n                    <img src=\"").concat(v.producturl, "\" width=\"100%\" />\n                    </figure>\n                    <p>").concat(v.productname, "</p>\n                </a>");
      }).join(""));
      this.productsBtn.attr("disabled", null);
      if (data.page * data.limit >= data.total) {
        this.productsBtn.remove();
      } else {
        this.productsBtn.html(this.loadedHTML);
      }
    },
    fetchProducts: function fetchProducts() {
      var _this = this;
      this.productNo++;
      this.productsBtn.attr("disabled", "disabled");
      this.productsBtn.html(this.loadingHTML);
      $.fetch({
        url: "/macProductPage",
        data: {
          pageSize: config.pageSize,
          pageNo: this.productNo
        }
      }).then(function (rs) {
        return _this.renderProducts(rs);
      });
    },
    renderCases: function renderCases(data) {
      this.cases.append(data.rows.map(function (v) {
        return "<a href=\"/caseInfo.html?id=".concat(v.id, "\" class=\"col-4 col-md-3 col-lg-2\">\n                    <figure class=\"font-0\">\n                    <img src=\"").concat(v.imgurl, "\" width=\"100\" height=\"100\"/>\n                    </figure>\n                    <p>").concat(v.proname, "</p>\n                </a>");
      }).join(""));
      this.casesBtn.attr("disabled", null);
      if (data.page * data.limit >= data.total) {
        this.casesBtn.remove();
      } else {
        this.casesBtn.html(this.loadedHTML);
      }
    },
    fetchCases: function fetchCases() {
      var _this2 = this;
      this.caseNo++;
      this.casesBtn.attr("disabled", "disabled");
      this.casesBtn.html(this.loadingHTML);
      $.fetch({
        url: "/macCasePage",
        data: {
          pageSize: config.pageSize,
          pageNo: this.caseNo
        }
      }).then(function (rs) {
        return _this2.renderCases(rs);
      });
    },
    init: function init() {
      var _this3 = this;
      $.fetch({
        url: "/macBannerList"
      }).then(function (rs) {
        _this3.renderSlider(rs || []);
      });
      this.fetchProducts();
      this.fetchCases();
    }
  };
  action.init();

}));
