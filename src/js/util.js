//全域配置
const config = (window.config = {
  pageSize: 6,
  baseUrl: "http://test.zcmy168.com"
});
// 判断变量否为function
const isFunction = variable => typeof variable === "function";
// 定义Promise的三种状态常量
const PENDING = "PENDING";
const FULFILLED = "FULFILLED";
const REJECTED = "REJECTED";

class MyPromise {
  constructor(handle) {
    if (!isFunction(handle)) {
      throw new Error("MyPromise must accept a function as a parameter");
    }
    // 添加状态
    this._status = PENDING;
    // 添加状态
    this._value = undefined;
    // 添加成功回调函数队列
    this._fulfilledQueues = [];
    // 添加失败回调函数队列
    this._rejectedQueues = [];
    // 执行handle
    try {
      handle(this._resolve.bind(this), this._reject.bind(this));
    } catch (err) {
      this._reject(err);
    }
  }

  // 添加resovle时执行的函数
  _resolve(val) {
    const run = () => {
      if (this._status !== PENDING) return;
      // 依次执行成功队列中的函数，并清空队列
      const runFulfilled = value => {
        let cb;
        while ((cb = this._fulfilledQueues.shift())) {
          cb(value);
        }
      };
      // 依次执行失败队列中的函数，并清空队列
      const runRejected = error => {
        let cb;
        while ((cb = this._rejectedQueues.shift())) {
          cb(error);
        }
      };
      /* 如果resolve的参数为Promise对象，则必须等待该Promise对象状态改变后,
              当前Promsie的状态才会改变，且状态取决于参数Promsie对象的状态
            */
      if (val instanceof MyPromise) {
        val.then(
          value => {
            this._value = value;
            this._status = FULFILLED;
            runFulfilled(value);
          },
          err => {
            this._value = err;
            this._status = REJECTED;
            runRejected(err);
          }
        );
      } else {
        this._value = val;
        this._status = FULFILLED;
        runFulfilled(val);
      }
    };
    // 为了支持同步的Promise，这里采用异步调用
    setTimeout(run, 0);
  }

  // 添加reject时执行的函数
  _reject(err) {
    if (this._status !== PENDING) return;
    // 依次执行失败队列中的函数，并清空队列
    const run = () => {
      this._status = REJECTED;
      this._value = err;
      let cb;
      while ((cb = this._rejectedQueues.shift())) {
        cb(err);
      }
    };
    // 为了支持同步的Promise，这里采用异步调用
    setTimeout(run, 0);
  }

  // 添加then方法
  then(onFulfilled, onRejected) {
    const { _value, _status } = this;
    // 返回一个新的Promise对象
    return new MyPromise((onFulfilledNext, onRejectedNext) => {
      // 封装一个成功时执行的函数
      let fulfilled = value => {
        try {
          if (!isFunction(onFulfilled)) {
            onFulfilledNext(value);
          } else {
            let res = onFulfilled(value);
            if (res instanceof MyPromise) {
              // 如果当前回调函数返回MyPromise对象，必须等待其状态改变后在执行下一个回调
              res.then(onFulfilledNext, onRejectedNext);
            } else {
              //否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
              onFulfilledNext(res);
            }
          }
        } catch (err) {
          // 如果函数执行出错，新的Promise对象的状态为失败
          onRejectedNext(err);
        }
      };
      // 封装一个失败时执行的函数
      let rejected = error => {
        try {
          if (!isFunction(onRejected)) {
            onRejectedNext(error);
          } else {
            let res = onRejected(error);
            if (res instanceof MyPromise) {
              // 如果当前回调函数返回MyPromise对象，必须等待其状态改变后在执行下一个回调
              res.then(onFulfilledNext, onRejectedNext);
            } else {
              //否则会将返回结果直接作为参数，传入下一个then的回调函数，并立即执行下一个then的回调函数
              onFulfilledNext(res);
            }
          }
        } catch (err) {
          // 如果函数执行出错，新的Promise对象的状态为失败
          onRejectedNext(err);
        }
      };
      switch (_status) {
        // 当状态为pending时，将then方法回调函数加入执行队列等待执行
        case PENDING:
          this._fulfilledQueues.push(fulfilled);
          this._rejectedQueues.push(rejected);
          break;
        // 当状态已经改变时，立即执行对应的回调函数
        case FULFILLED:
          fulfilled(_value);
          break;
        case REJECTED:
          rejected(_value);
          break;
      }
    });
  }

  // 添加catch方法
  catch(onRejected) {
    return this.then(undefined, onRejected);
  }

  // 添加静态resolve方法
  static resolve(value) {
    // 如果参数是MyPromise实例，直接返回这个实例
    if (value instanceof MyPromise) return value;
    return new MyPromise(resolve => resolve(value));
  }

  // 添加静态reject方法
  static reject(value) {
    return new MyPromise((resolve, reject) => reject(value));
  }
}

window.Promise = window.Promise || MyPromise;

const bodyEl = $(document.body);
const modal = (window.modal = {
  point: null,
  base(cfg) {
    if (this.point) {
      this.cancel(this.point);
    }
    const modal = $(
      '<div class="fixed full-width full-height left top row bg-translucence modal text-center"></div>'
    );
    let dom = `<div class="self-center bg-white round-xs">
                <div class="modal-body pd-sm line-1">`;
    if (cfg.type === "loading") {
      dom += `<div class="loading row justify-between self-center"><i class="iconfont iconloading fg-black font-24"></i></div></div>`;
    } else if (cfg.type === "confirm") {
      dom += `${cfg.msg}</div>
                <div class="modal-footer row btn-group">
                    <button class="btn bg-orange col-6" onclick="modal.cancel_cb()">取消</button>
                    <button class="btn bg-green col-6" onclick="modal.ok()">确定</button>
                </div>`;
      this.cancel = cfg.cancel;
      this.confirm = cfg.confirm;
    }
    dom += `</div>
        </div>`;
    modal.append(dom);
    bodyEl.append(modal);
    setTimeout(() => modal.addClass("show"), 60);
    this.point = modal;
  },
  cancel_cb(el) {
    el = el || this.point;
    el.removeClass("show");
    this.cancel && this.cancel();
    setTimeout(() => {
      el.remove();
      el = this.point = null;
    }, 200);
  },
  cancel: null,
  ok: null
});

$.loading = () => modal.base({ type: "loading" });
$.confirm = cfg =>
  modal.base({
    type: "confirm",
    msg: cfg.msg || "",
    cancel: cfg.cancel,
    ok: cfg.ok
  });
$.hide = () => modal.cancel_cb(modal.point);

const toast = (window.toast = {
  el: null,
  base(flag, msg, time) {
    msg = msg || (flag ? "操作成功" : "操作失败");
    if (this.el) {
      this.el.remove();
    }
    this.el = $(
      `<div class="notice fixed left top full-width row items-center pad-sm-x pad-xs-y shadow-xs-down ellipsis bg-white justify-center"></div>`
    );
    this.el.append(`<i class="pad-xs-r font-20 iconfont ${
      flag ? "iconchenggong fg-green" : "icontishi fg-orange"
    }"></i>
        <span>&nbsp;${msg}</span>`);
    bodyEl.append(this.el);
    setTimeout(() => this.el.addClass("show"));
    setTimeout(() => this.cancel(), time || 2500);
  },
  cancel(el) {
    el = el || this.el;
    el.removeClass("show");
    setTimeout(() => el.remove(), 200);
  }
});
$.success = (msg, time) => toast.base(1, msg, time);
$.error = (msg, time) => toast.base(0, msg, time);

$.fetch = function(o = {}) {
  return new Promise((resolve, reject) => {
    $.ajax({
      timeout: 20000,
      type: o.type || "GET",
      url: config.baseUrl + o.url,
      data: o.data || {},
      dataType: "json",
      contentType: "application/json",
      success(data, status, xhr) {
        if (data.code === 1) {
          resolve(data.data);
        } else {
          $.error(data.msg);
          reject(data);
        }
      },
      error(xhr, errorType, error) {
        if (errorType === "timeout")
          return $.confirm({
            msg: "请求失败，是否重试？",
            ok() {
              $.loading();
              $.fetch(o);
            }
          });
        $.error("网络错误！");
      }
    });
  });
};

$.getUrlParams = function() {
  let params = {};
  const _params = window.location.search
    .replace("?", "")
    .split("&")
    .forEach(v => {
      const kv = v.split("=");
      params[kv[0]] = kv[1];
    });
  return params;
};
