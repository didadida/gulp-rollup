const action = (window.action = {
  slider: $(".slide-box"),
  products: $(".products"),
  productsBtn: $(".products-btn"),
  cases: $(".cases"),
  casesBtn: $(".cases-btn"),
  productNo: 0,
  caseNo: 0,
  loadingHTML: `<i class="iconfont iconloading inline mg-xs-r"></i>正在加载...`,
  loadedHTML: `查看更多`,
  renderSlider(data) {
    this.slider.html(
      data
        .map((v, i) => {
          return `
            <li class="absolute left top full-width full-height">
                <a href="${v.clickUrl}">
                <img ${i == 0 ? "" : "data-"}src="${v.imgUrl}"/>
                </a>
            </li>
            `;
        })
        .join("")
    );
    setTimeout(function() {
      $("#slide").swipeSlide({
        autoSwipe: true,
        continuousScroll: true,
        lazyLoad: true
      });
    }, 0);
  },

  renderProducts(data) {
    this.products.append(
      data.rows
        .map(v => {
          return `<a href="/productInfo.html?id=${
            v.id
          }" class="col-4 col-md-3 col-lg-2">
                    <figure class="font-0">
                    <img src="${v.producturl}" width="100%" />
                    </figure>
                    <p>${v.productname}</p>
                </a>`;
        })
        .join("")
    );
    this.productsBtn.attr("disabled", null);
    if (data.page * data.limit >= data.total) {
      this.productsBtn.remove();
    } else {
      this.productsBtn.html(this.loadedHTML);
    }
  },
  fetchProducts() {
    this.productNo++;
    this.productsBtn.attr("disabled", "disabled");
    this.productsBtn.html(this.loadingHTML);
    $.fetch({
      url: "/macProductPage",
      data: {
        pageSize: config.pageSize,
        pageNo: this.productNo
      }
    }).then(rs => this.renderProducts(rs));
  },
  renderCases(data) {
    this.cases.append(
      data.rows
        .map(v => {
          return `<a href="/caseInfo.html?id=${
            v.id
          }" class="col-4 col-md-3 col-lg-2">
                    <figure class="font-0">
                    <img src="${v.imgurl}" width="100" height="100"/>
                    </figure>
                    <p>${v.proname}</p>
                </a>`;
        })
        .join("")
    );
    this.casesBtn.attr("disabled", null);
    if (data.page * data.limit >= data.total) {
      this.casesBtn.remove();
    } else {
      this.casesBtn.html(this.loadedHTML);
    }
  },
  fetchCases() {
    this.caseNo++;
    this.casesBtn.attr("disabled", "disabled");
    this.casesBtn.html(this.loadingHTML);
    $.fetch({
      url: "/macCasePage",
      data: {
        pageSize: config.pageSize,
        pageNo: this.caseNo
      }
    }).then(rs => this.renderCases(rs));
  },

  init() {
    $.fetch({
      url: "/macBannerList"
    }).then(rs => {
      this.renderSlider(rs || []);
    });
    this.fetchProducts();
    this.fetchCases();
  }
});
action.init();
